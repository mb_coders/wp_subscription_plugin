<?php

namespace WP_SUBSCRIPTION_PLUGIN;

/**
 * This class defines all code necessary to run during the plugin's activation.
 */
class Activator
{
    /**
     * Sets the default options in the options table on activation.
     */
    public static function activate() {
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-cron.php';

        $option_name = INFO::OPTION_NAME;
        if (get_option($option_name) == false) {
            $default_options = [
                // 'key' => 'value',
            ];
            update_option($option_name, $default_options);
        }

        if (! wp_next_scheduled ( 'two_months_event' )) {
            wp_schedule_event(time(), 'daily', 'two_months_event');
        }

        wp_schedule_event(time(), 'daily', 'prolongation_event');

        $cron = new Cron();
        add_action('two_months_event', [$cron, 'new_sub_order']);
        add_action('prolongation_event', [$cron, 'prolongation_email']);
    }
}
