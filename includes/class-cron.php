<?php

namespace WP_SUBSCRIPTION_PLUGIN;

/**
* This class defines cron jobs
*/
class Cron
{
	public function new_sub_order() {
		$date = date('j');
		if ($date != get_field('dzien_wysylki', 'option')) return;

		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/controllers/subscriptions_controller.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/controllers/issue_controller.php';

		$sub_contr = new Subscriptions();
		$issue_contr = new Issues();

		$query = $sub_contr->search('papierowa', 'tak');

		if ($query->have_posts()) {
			while ($query->have_posts()){
				$query->the_post();

				$all = get_field('ilosc_wszystkich_numerow');
				$sent = get_field('ilosc_wyslanych_numerow');

				if ($sent < $all) {
					$first_issue = explode("/", get_field('pierwszy_numer'));
					$first_issue = [ 'month' => $first_issue[0], 'year' => $first_issue[1] ];

					$next_issue_id = $issue_contr->next_issue($first_issue, 'id');
					
					//Pobranie danych do zamówienia
					$address = array(
						'first_name' => get_field('imie'),
						'last_name'  => get_field('nazwisko'),
						'company'    => get_field('firma'),
						'email'      => get_field('email'),
						'address_1'  => get_field('adres_linia_1'),
						'address_2'  => get_field('adres_linia_2'),
						'city'       => get_field('miasto'),
						'postcode'   => get_field('kod_pocztowy'),
						'country'    => get_field('kraj')
					);

					//Wygenerowanie zamówienia
					$order = wc_create_order();
					$order->add_product( get_product( $next_issue_id ), 1);
					$order->set_address( $address, 'billing' );
					$order->calculate_totals();
					$order->update_status("Completed", 'Automatycznie wygenerowane z prenumeraty', TRUE);

					//Flaga, że jest to zamówienie z subskrypcji
					update_field('wygenerowane_z_prenumeraty', true, $order->get_id());
				}
			}
		}

		wp_reset_query();
	}

	public function prolongation_email() {
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/controllers/subscriptions_controller.php';

		$sub_contr = new Subscriptions();
		$query = $sub_contr->search('', 'tak');

		while ($query->have_posts()){
			$query->the_post();

			$email_status = get_field('przypomnienie_o_zakonczeniu_prenumeraty');
			$all_issues = get_field('ilosc_wszystkich_numerow');
			$sent_issues = get_field('ilosc_wyslanych_numerow');

			if ($email_status == 'nie' && $all_issues - $sent_issues == 2) {
				//4 miesiące przed zakończeniem
				do_action( 'woocommerce_prolongation', [$post->ID, 4] );
				update_field('przypomnienie_o_zakonczeniu_prenumeraty', '4');
			}elseif ($email_status == '4' && $all_issues - $sent_issues == 1) {
				//2 miesiące przed zakończeniem
				do_action( 'woocommerce_prolongation', [$post->ID, 2] );
				update_field('przypomnienie_o_zakonczeniu_prenumeraty', '2');
			}
		}
		wp_reset_query();
	}
}

?>