<?php

namespace WP_SUBSCRIPTION_PLUGIN;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * A custom WooCommerce Email class
 *
 * @since 0.1
 * @extends \WC_Email
 */
class WC_Prolongation_Order_Email extends \WC_Email {

	public function __construct() {

        // set ID, this simply needs to be a unique name
        $this->id = 'wc_prolongation_order';

        // this is the title in WooCommerce Email settings
        $this->title = 'Przedłużenie prenumeraty';

        // this is the description in WooCommerce email settings
        $this->description = 'Email o przedłużenie prenumeraty jest wysyłany automatycznie 2 i 4 miesiące przed zakończeniem prenumeraty.';

        // these are the default heading and subject lines that can be overridden using the settings
        $this->heading = 'Przedłużenie prenumeraty';
        $this->subject = 'Przedłużenie prenumeraty';

        // these define the locations of the templates that this email should use, we'll just use the new order template since this email is similar
        $this->template_html  = 'emails/prolongation.php';
        $this->template_plain = 'emails/plain/admin-new-order.php';

        // Trigger on new paid orders
        /////////  DAĆ TRIGGER DO CRONA !!!! /////////////
        add_action( 'woocommerce_prolongation', array( $this, 'trigger' ) );

        // Call parent constructor to load any other defaults not explicity defined here
        parent::__construct();

        // this sets the recipient to the settings defined below in init_form_fields()
        $this->recipient = $this->get_option( 'recipient' );

        // if none was entered, just use the WP admin email as a fallback
        if ( ! $this->recipient )
            $this->recipient = get_option( 'admin_email' );
    }

    /**
     * Determine if the email should actually be sent and setup email merge variables
     *
     * @since 0.1
     * @param int $sub id
     * @param $type 2 | 4
     */
    public function trigger( $sub_id, $type ) {

        // bail if no ID or type is present
        if ( ! $order_id || ! $type)
            return;        

        if ( ! $this->is_enabled() )
            return;

        $content = $this->get_content();
        $email_content = ($type == 2)? $this->get_option('content-2') : $this->get_option('content-4');
        $content = preg_replace('/{order_content}/', $email_content, $content);

        // woohoo, send the email!
        $this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
    }

    /**
     * get_content_html function.
     *
     * @since 0.1
     * @return string
     */
    public function get_content_html() {
        ob_start();
        woocommerce_get_template( $this->template_html, array(
            'order'         => $this->object,
            'email_heading' => $this->get_heading()
        ) );
        return ob_get_clean();
    }


    /**
     * get_content_plain function.
     *
     * @since 0.1
     * @return string
     */
    public function get_content_plain() {
        ob_start();
        woocommerce_get_template( $this->template_plain, array(
            'order'         => $this->object,
            'email_heading' => $this->get_heading()
        ) );
        return ob_get_clean();
    }

    /**
     * Initialize Settings Form Fields
     *
     * @since 0.1
     */
    public function init_form_fields() {

        $this->form_fields = array(
            'enabled'    => array(
                'title'   => 'Enable/Disable',
                'type'    => 'checkbox',
                'label'   => 'Enable this email notification',
                'default' => 'yes'
            ),
            'subject'    => array(
                'title'       => 'Tytuł',
                'type'        => 'text',
                'description' => sprintf( 'Tytuł wiadomości', $this->subject ),
                'placeholder' => '',
                'default'     => ''
            ),
            'heading'    => array(
                'title'       => 'Nagłówek',
                'type'        => 'text',
                'description' => sprintf( __( 'Nagłówek w wiadomości.' ), $this->heading ),
                'placeholder' => '',
                'default'     => ''
            ),
            'content-2'    => array(
                'title'       => 'Treść',
                'type'        => 'textarea',
                'description' => sprintf( 'Treść wiadomości wysyłanej 2 miesiące przed zakończeniem.' ),
                'placeholder' => '',
                'default'     => ''
            ),
            'content-4'    => array(
                'title'       => 'Treść',
                'type'        => 'textarea',
                'description' => sprintf( 'Treść wiadomości wysyłanej 4 miesiące przed zakończeniem.' ),
                'placeholder' => '',
                'default'     => ''
            ),
        );
    }

}