<?php

namespace WP_SUBSCRIPTION_PLUGIN;

/**
 * The main plugin class.
 */
class Plugin
{

    private $loader;
    private $plugin_slug;
    private $version;
    private $option_name;

    public function __construct() {
        $this->plugin_slug = Info::SLUG;
        $this->version     = Info::VERSION;
        $this->option_name = Info::OPTION_NAME;
        $this->load_dependencies();
        $this->define_admin_hooks();
        $this->define_frontend_hooks();
        $this->define_shortcodes();
    }

    private function load_dependencies() {
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-loader.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-admin.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'frontend/class-frontend.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-shortcodes.php';
        $this->loader = new Loader();
    }

    private function define_shortcodes() {
        $shortcodes = new Shortcodes();
        add_shortcode( 'subscription_reminder', [$shortcodes, 'subscription_reminder'] );
    }

    private function define_admin_hooks() {
        $plugin_admin = new Admin($this->plugin_slug, $this->version, $this->option_name);
        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'assets');
        $this->loader->add_action( 'admin_init', $plugin_admin, 'register_settings');
        $this->loader->add_action( 'admin_menu', $plugin_admin, 'add_menus');
        $this->loader->add_action( 'init', $plugin_admin, 'reg_new_wsk_taxonomy'  );
        $this->loader->add_action( 'init', $plugin_admin, 'new_wsk_terms'  );
        $this->loader->add_action( 'init', $plugin_admin, 'reg_post_type_wsk_prenumeraty'  );
        $this->loader->add_action( 'init', $plugin_admin, 'acf_options'  );
        $this->loader->add_action( 'admin_init', $plugin_admin, 'check_config'  );
        $this->loader->add_action( 'manage_wsk_prenumeraty_posts_custom_column' , $plugin_admin, 'custom_columns_data', 10, 2 );
        $this->loader->add_action( 'manage_shop_order_posts_custom_column' , $plugin_admin, 'order_columns_data', 10, 2 ); 
        $this->loader->add_action( 'woocommerce_product_after_variable_attributes', $plugin_admin, 'add_custom_general_fields' , 10, 3 );
        $this->loader->add_action( 'woocommerce_save_product_variation', $plugin_admin, 'add_custom_general_fields_save' , 10, 2 );
        $this->loader->add_action( 'restrict_manage_posts', $plugin_admin, 'new_filters'  );
        $this->loader->add_action( 'woocommerce_order_status_completed', $plugin_admin, 'create_prenumerata' , 10, 1 );
        $this->loader->add_action( 'bulk_edit_custom_box', $plugin_admin, 'add_quick_edit' , 10, 3);
        $this->loader->add_action( 'save_post', $plugin_admin, 'save_bulk_edit_data' );
        $this->loader->add_action( 'admin_notices', $plugin_admin, 'export_btn' );
        $this->loader->add_action( 'admin_notices', $plugin_admin, 'generate_orders_from_subs' );
        $this->loader->add_action( 'wp_loaded', $plugin_admin, 'generate_issues' );

        $this->loader->add_filter( 'acf/settings/save_json', $plugin_admin, 'acf_save'  );
        $this->loader->add_filter( 'acf/settings/load_json', $plugin_admin, 'acf_load' );
        $this->loader->add_filter( 'manage_wsk_prenumeraty_posts_columns', $plugin_admin, 'custom_columns_labels' );
        $this->loader->add_action( 'manage_edit-shop_order_columns' , $plugin_admin, 'orders_columns_labels' );
        $this->loader->add_filter( 'manage_edit-wsk_prenumeraty_sortable_columns', $plugin_admin, 'sortable_columns' );
        $this->loader->add_filter( 'acf/load_field/name=najstarszy_numer', $plugin_admin, 'update_select_issues' );
        $this->loader->add_filter( 'acf/load_field/name=pierwszy_numer', $plugin_admin, 'update_select_issues' );
        $this->loader->add_filter( 'acf/load_field/name=ostatni_wyslany_numer', $plugin_admin, 'update_select_issues' );
        $this->loader->add_filter( 'page_row_actions', $plugin_admin, 'remove_row_actions' , 10, 2 );
        $this->loader->add_filter( 'post_row_actions', $plugin_admin, 'remove_row_actions' , 10, 2 );
        $this->loader->add_filter( 'parse_query', $plugin_admin, 'posts_filter'  );
        $this->loader->add_filter( 'parse_query', $plugin_admin, 'orders_filter'  );
        $this->loader->add_filter( 'woocommerce_email_classes', $plugin_admin, 'prolongation_sub_woocommerce_email'  );
    }

    private function define_frontend_hooks() {
        $plugin_frontend = new Frontend($this->plugin_slug, $this->version, $this->option_name);
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_frontend, 'assets');
        $this->loader->add_action( 'wp_footer', $plugin_frontend, 'render');
        $this->loader->add_action( 'woocommerce_before_add_to_cart_button', $plugin_frontend, 'before_add_to_cart_button', 9);
        $this->loader->add_action( 'woocommerce_add_cart_item_data', $plugin_frontend, 'new_order_item', 10, 2);
        $this->loader->add_action( 'woocommerce_add_order_item_meta', $plugin_frontend, 'add_order_item_meta', 10, 2 );

        $this->loader->add_filter( 'woocommerce_get_cart_item_from_session', $plugin_frontend, 'get_cart_item_from_session', 20, 2 );
        $this->loader->add_filter( 'woocommerce_get_item_data', $plugin_frontend, 'get_item_data', 10, 2 );
        $this->loader->add_filter( 'woocommerce_order_item_product', $plugin_frontend, 'order_item_product', 10, 2 );    
    }

    public function run() {
        $this->loader->run();
    }
}
