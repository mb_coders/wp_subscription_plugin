<?php

namespace WP_SUBSCRIPTION_PLUGIN;

/**
 * Subscriptions controller
 */
class Subscriptions
{
    /*
    * Add new subscription
    */
	public function add ( $typ, $active, $fields ) {
		$postarr = array(
            'post_title'    =>  '',
            'post_type'     =>  'wsk_prenumeraty',
            'post_status'   =>  'publish',
        );
        $sub_id = wp_insert_post( $postarr );

        wp_set_object_terms( $sub_id, $typ, 'wsk_typ' );
        wp_set_object_terms( $sub_id, $active, 'wsk_active' );

        foreach ($fields as $key => $value) {
        	update_field($key, $value, $sub_id);
        }

        return $sub_id;
	}

    /*
    * Update subscription
    */
	public function update ( $sub_id, $typ, $active, $fields ) {
		wp_set_object_terms( $sub_id, $typ, 'wsk_typ' );
        wp_set_object_terms( $sub_id, $active, 'wsk_active' );
        
		foreach ($fields as $key => $value) {
        	update_field($key, $value, $sub_id);
        }

        return true;
	}

    /*
    * Check if subscription exist by ID
    */
    public function is_exist ( $sub_id ) {
        $args = array(
            'post_type' => 'wsk_prenumeraty',
            'p' => $sub_id,
        );

        $query = new \WP_Query($args);

        if ($query->have_posts()){
            return true;
        }else{
            return false;
        }
    }

	/**
	* Quick search subscriptions by type, active and meta_key
	* Remember to clean search with wp_reset_query()!
	* @return WP_Query
	*/
	public function search ( $type = '', $active = '', $meta_key = '', $meta_value = '' ) {
		$args = array( 'post_type'=>'wsk_prenumeraty' );

		$tax_query = array();

		if ($type != '') {
			$tax_query[] = array(
				'taxonomy'=>'wsk_typ',
                'field'=>'slug',
                'terms'=>$type
				);
		}

		if ($active != '') {
			$tax_query[] = array(
				'taxonomy'=>'wsk_active',
                'field'=>'slug',
                'terms'=>$active
				);
		}

		if ($meta_key != '' && $meta_value != '') {
			$args['meta_key'] = $meta_key;
			$args['meta_value'] = $meta_value;
		}

		$args['tax_query'] = $tax_query;

        $query = new \WP_Query($args);

        return $query;
	}

    /*
    * Get end date of subscription
    */
    public function end ( $sub_id ) {
        $first_issue = get_field('pierwszy_numer');
        $all_issues = get_field('ilosc_wszystkich_numerow');

        $first_arr = explode('/', $first_issue);
        $first_date = [ 'month' => $first_arr[0], 'year' => $first_arr[1] ];

        $last_date = [ 'month' => $first_date['month'] + $all_issues, 'year' => $first_date['year'] ];
        
        while ($last_date['month'] > 12) {
            $last_date['month'] -= 12;
            $last_date['year']++;
        }

        return $last_date;
    }
}