<?php

namespace WP_SUBSCRIPTION_PLUGIN;

/**
 * Issues controller
 */
class Issues
{
    /*
    * Add new issue
    */
    public function add ( $month, $year, $virtual = false ) {
        $numer = floor($month / 2) + 1;
        $issue_id = wp_insert_post( array(
            'post_type' => 'product',
            'post_status' => 'draft',
            'post_title' => $numer.'/'.$year,
        ), true );

        wp_set_object_terms( $issue_id, 'numer-magazynu', 'wsk_typ' );

        update_field('data_magazynu', date('Ymd', strtotime('1-'.$month.'-'.$year)), $issue_id);
        if ($virtual){
            update_post_meta( $post_id, '_virtual', 'yes');
        }

        return $issue_id;
    }

    /*
    * Get issue by date
    * @return issue ID
    */
    public function get_by_date ( $month, $year ) {
        $args = array(
            'post_type' => 'product',
            'tax_query' => array(
                array(
                    'taxonomy' => 'wsk_typ',
                    'field' => 'slug',
                    'terms' => 'numer-magazynu',
                ),
            ),
            'meta_query' => array(
                array(
                    'key' => 'data_magazynu',
                    'value' => date('Ymd', strtotime($month.'-'.$year)),
                    'type' => 'DATE',
//                    'value' => $month . '/' . $year,
                    'compare' => '=',
                ),
            ),
        );

        $query = new \WP_Query($args);

        if ($query->have_posts()) {
            $query->the_post();
            
            $issue_id = get_the_ID();
            wp_reset_query();

            return $issue_id;
        }else{
            return false;
        }
    }

    /*
    * Get current issue
    * $return 'id' / 'date'
    */
    public function get_current_issue ( $return = 'id' ) {
        $month = date('n');
        $issue_month = ($month % 2 == 0)? $month -1 : $month;
        $issue_year = date('Y');

        $args = array(
            'post_type' => 'product',
            'post_status' => ['public', 'draft'],
            'tax_query' => array(
                array(
                    'taxonomy' => 'wsk_typ',
                    'field' => 'slug',
                    'terms' => 'numer-magazynu',
                ),
            ),
            'meta_query' => array(
                array(
                    'key' => 'data_magazynu',
                    'value' => date('Ymd', strtotime($issue_month.'-'.$issue_year)),
                    'type' => 'DATE',
                    'compare' => '=',
                ),
            ),
        );
        $query = new \WP_Query($args);
        if ($query->have_posts()) {
            $query->the_post();

            if ($return == 'id') {
                return get_the_ID();
            }else{
                return array('month' => $issue_month, 'year' => $issue_year);
            }
        }else {
            return false;
        }
    }

    /*
    * Get last existing issue
    * $return 'id' / 'date'
    */
    public function get_last_issue ( $return = 'id' ) {
        $args = array(
            'post_type' => 'product',
            'post_status' => ['public', 'draft'],
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'wsk_typ',
                    'field' => 'slug',
                    'terms' => 'numer-magazynu',
                ),
            ),
        );
        $query = new \WP_Query($args);

        $last_month = 0;
        $last_year = 0;
        $id = -1;

        if ($query->have_posts()){
            while ($query->have_posts()){
                $query->the_post();

                $date = get_field('data_magazynu');
                $month = date('n', strtotime($date));
                $year = date('Y', strtotime($date));

                if ( ($year > $last_year) || ($year == $last_year && $month > $last_month) ) {
                    $last_month = $month;
                    $last_year = $year;
                    $id = get_the_ID();
                }
            }
        }else{
            return false;
        }
        wp_reset_query();

        if ($return == 'id') {
            return $id;
        }else{
            if ($last_month == 0 || $last_year == 0){
                return false;
            }else{
                return array('month' => $last_month, 'year' => $last_year);
            }
        }
    }

    /*
    * Diff between two issues
    */
    public function issues_diff ( $issue1, $issue2 ) {
        $year_diff = ($issue2['year'] - $issue1['year']) * 6;

        if ($issue2['month'] >= $issue1['month']){
            $month_diff = ($issue2['month'] - $issue1['month']) / 2;
            return $year_diff + $month_diff;
        }else{
            $month_diff = ($issue1['month'] - $issue2['month']) / 2;
            return $year_diff - $month_diff;
        }
    }

    /*
    * Get next issue
    * $return 'id' / 'date'
    */
    public function next_issue ( $current_issue, $return = 'id' ) {
        if ($return == 'date') {
            $next_month = $current_issue['month'] + 2;
            $next_year = $current_issue['year'];

            if ($next_month > 12) {
                $next_month -= 12;
                $next_year++;
            }

            return array('month' => $next_month, 'year' => $next_year);
        } else {
            $next_month = $current_issue['month'] + 1;
            $next_year = $current_issue['year'];
            if ($next_month > 6) {
                $next_month = 1;
                $next_year++;
            }            

            return $this->get_by_date($next_month, $next_year);
        }
    }
}