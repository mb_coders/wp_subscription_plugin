<?php

namespace WP_SUBSCRIPTION_PLUGIN;

/**
* Shortcodes
*/
class Shortcodes
{
	public static function subscription_reminder( $atts ) {
		$atts = shortcode_atts( array(
			'placeholder' => 'E-mail',
			'submit' => 'Wyślij'
		), $atts, 'subscription_reminder' );

		extract($atts);

		$html = "<form id=\"subscription_reminder\" method=\"post\">
			<input type=\"email\" name=\"email\" placeholder=\"{$placeholder}\" />
			<input type=\"submit\" value=\"{$submit}\" />
		</form>";

		if (isset($_POST) && !empty($_POST)) {
			$email = $_POST['email'];
			
			if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
				require_once plugin_dir_path(dirname(__FILE__)) . 'includes/controllers/subscriptions_controller.php';

				$sub_contr = new Subscriptions();

				$subject = "Stan prenumeraty";

				$query = $sub_contr->search('', '', 'email', $email);

				if ($query->have_posts()) {
					//Jest prenumerata
					$query->the_post();
					$terms = get_the_terms($post->ID, 'wsk_active');
					foreach ($terms as $term) {
						if ($term->slug == 'tak') {
							$active = true;
						}else {
							$active = false;
						}
					}

					if ($active) {
						$data = $sub_contr->end($post->ID);
						$message = "Twoja prenumerata jest aktywna do {$data["month"]}/{$data["year"]}";
					}else{
						$message = "Twoja prenumerata nie jest już aktywna.";
					}

				}else {
					//Brak prenumeraty
					$message = "Na podany adres email nie ma żadnej prenumeraty.";
				}
				wp_reset_query();

				wp_mail( $email, $subject, $message );

				$html .= '<p id="subscription_reminder_success">Wiadomość została wysłana!</p>';
			} else {
				$html .= '<p id="subscription_reminder_error">Podany adres e-mail jest nie poprawny.</p>';
			}
		}

		return $html;
	}
}

?>