<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="user-scalable=no,width=device-width" />
<title>Init Database</title>
</head>
<body>
<?php 
require_once "../settings.php";

$found=false;
$tableName = $db_tablename;

if (mysql_connect($db_host, $db_user, $db_pass) && mysql_select_db($db_name) )  {
	
	$query = "CREATE TABLE  IF NOT EXISTS `op_subscriptions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `emailAddress` varchar(50) NOT NULL DEFAULT '',
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `subscriptionStart` date NOT NULL,
  `subscriptionRenew` date DEFAULT NULL,
  `active` varchar(12) NOT NULL DEFAULT '1',
  `authToken` varchar(100) DEFAULT NULL,
  `uuids` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `emailAddress` (`emailAddress`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	
	$result = mysql_query($query);
	if (!$result) {
		$msg = "Could not create table: ". $tableName;
		$logger->logError($msg);
		echo $msg;
	}
	else {
		$msg = "Created table: " . $tableName;
		$logger->logInfo($msg);
		echo $msg;
	}
}
else {
	$e = "Could not connect to database: " . mysql_error();
	$logger->logError($e );
	echo $e;
}
?>
</body></html>

