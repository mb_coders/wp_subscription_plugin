<?php
require_once "../settings.php";
require_once "../utils.php";

// Gather POST variables.  For ease of testing, we are agnostic of http method used.
$firstName    = $_REQUEST['firstName'];
$lastName     = $_REQUEST['lastName'];
$emailAddress = $_REQUEST['emailAddress'];
$password     = MD5($_REQUEST['password']);
$password1    = MD5($_REQUEST['password1']);

$success=false;
$info = "";

if ($password==$password1) {
	if (validUsername($emailAddress) ) {
		if (mysql_connect($db_host, $db_user, $db_pass) && mysql_select_db($db_name)) {
			date_default_timezone_set("UTC");
			
			$substart = date("Y-m-d");
			$subrenew = date("Y-m-d", time()+31*24*60*60);
			$query = sprintf("INSERT INTO %s (firstName, lastname, emailAddress, password, subscriptionStart,  subscriptionRenew, active) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '1')",
				$db_tablename,
				escapeURLData($firstName),
				escapeURLData($lastName),
				escapeURLData($emailAddress),
				escapeURLData($password),
				escapeURLData($substart),
				escapeURLData($subrenew)
				);
				
			if (mysql_query($query)) {
				$success=true;
			}
			else {
				$info = "error inserting new user: ". $emailAddress . " mysql: ".mysql_error();
			}
		}
		else {
			$info = "error access database: ".mysql_error();
		}
	}
	else {
		$info = "The username provided must be an email or any string containing \"A-z, 0-9, _, -, +, or .\" between 3 and 64 characters long.";
	}
}
else {
	$info = "passwords do not match";
}

if (!empty($info)) $logger->logInfo($info);

if ($success) {
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Account Created</title>
<style>
body {
	font-size:24px;
	color:#FFFFFF;
	font-family:Verdana, Geneva, sans-serif;
	background-color: #999;
}
</style>
</head>
<body>
<P>Thank you for registering</P>
</body>
</html>
<?php
}
else {
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Could not create account</title>
<style>
body {
	font-size:24px;
	color:#FFFFFF;
	font-family:Verdana, Geneva, sans-serif;
	background-color: #999;
}
</style>
</head>
<body>
<P>Sorry.  <?php echo $info; ?></P>
</body>
</html>
<?php
}
?>
