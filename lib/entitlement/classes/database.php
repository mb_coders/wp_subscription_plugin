<?php

include_once '../settings.php';
include_once 'subscriber.php';
include_once 'application.php';

class Database {
	
	protected $db;
	protected $appId;
	
	public function __construct($appId) {
		if (empty($appId)) die("you must provide an appId");
		
		global $db_name, $db_host, $db_pass, $db_user;
		
		$this->appId = $appId;
		$this->accountIsStage = false;
		
		//error_log("connecting to databse '".$db_name."' on ".$db_host);
		
		$this->db = new mysqli($db_host, $db_user, $db_pass, $db_name);
		if ($this->db->connect_errno) {
			die("Could not initiliaze database::".mysql_error());
		}
		error_log("database for ".$appId." created.");
	}
	
	static public function Create() {
		global $db_name, $db_host, $db_pass, $db_user;
		global $db_applications, $db_tablename;
		
		// Create database
		$db = new mysqli($db_host, $db_user, $db_pass);
		$sql = "Create database IF NOT EXISTS ".$db_name;
		if (!$db->query($sql)) {
			die( "error creating database - ".$db->error);
		}
		
		$db->select_db($db_name);
		
		// Create applications table
		$sql = "CREATE TABLE IF NOT EXISTS `".$db_applications."` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `appid` varchar(50) NOT NULL default '',
  `guid` varchar(50) NOT NULL default '',
  `adobeid` varchar(50) default NULL,
  `offer` varchar(15) default NULL,
  `environment` varchar(15) default 'production',
  `integrator` varchar(50) default NULL,
  `description` varchar(50) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `appid` (`appid`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;";
		if (!$db->query($sql)) {
			die( "error creating table 1 (".$db_applications.") - ".$db->error);
		}
		
		// Create subscriptions table
		$sql = "CREATE TABLE IF NOT EXISTS `".$db_tablename."` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `emailAddress` varchar(50) DEFAULT NULL,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `subscriptionStart` date DEFAULT NULL,
  `subscriptionRenew` date DEFAULT NULL,
  `active` varchar(12) NOT NULL DEFAULT '1',
  `authToken` varchar(100) DEFAULT NULL,
  `uuids` longtext,
  `appid` varchar(50) DEFAULT NULL,
  `offer` varchar(15) DEFAULT NULL,
  `productid` varchar(50) DEFAULT NULL,
  `integrator` tinyint(1) DEFAULT NULL,
  `filter` varchar(50) DEFAULT NULL,
  `verifiedProducts` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;";
		if (!$db->query($sql)) {
			die( "error creating table 2 (".$db_tablename.") - ".$db->error);
		}
		
		return true;
	}
	
	
	
	public function getIntegratorId() {
		
		$sql = "select * from ".$GLOBALS['db_applications']." where appid='".$this->appId."'";
		if ($result = $this->db->query($sql)) {
			$row = $result->fetch_assoc();
			error_log("integrator = ".$row['integrator']);
			$this->accountIsStage = $row->isstage;
			return $row['integrator'];
		} else {
			error_log("mysql erro: "+mysql_error);
		}
		return null;
	}
	
	public function createApplication($application) {
		global $db_applications;
		
		$sql = "INSERT INTO ". $db_applications." (appid, guid, adobeid, offer, environment, integrator, description) VALUES ('".$application->appId."', '".$application->guid."', '".$application->adobeid."', '".$application->offer."', '".$application->environment."','".$application->integrator."','".$application->description."')";
		
		error_log("createApplication::".$sql);
		
		if ($this->db->query($sql)) return $this->db->insert_id;
		else return false;
	}
	
	public function getAllApplications() {
		global $db_applications;
		
		$r = array();
		
		if ($result = $this->db->query("select * from ".$db_applications)) {
			error_log("getAppApplications::count=".$result->num_rows);
			while ($row = $result->fetch_assoc()) {
				$application = new Application($row['appid']);
				$application->adobeid    = $row['adobeid'];
				$application->guid       = $row['guid'];
				$application->integrator = $row['integrator'];
				$application->environment= $row['environment'];
				$application->offer      = $row['offer'];
				$application->id         = $row['id'];
				$application->description= $row['description'];
				array_push($r, $application);
			}
		}
		else {
			error_log("mysql erro: "+$this->db->error);
		}
		return $r;
	}
	
	public function removeApplicationById($id) {
		global $db_applications;
		
		$sql = "DELETE FROM ".$db_applications." WHERE id=".$id;
		error_log($sql);
		
		return $this->db->query($sql);
	}
	
	public function getApplication() {
		global $db_applications;
		
		$sql = "select * from ".$db_applications." where appid='".$this->appId."'";
		if ($result = $this->db->query($sql)) {
			$row = $result->fetch_assoc();
			
			$application = new Application($this->appId);
			$application->adobeid    = $row['adobeid'];
			$application->guid       = $row['guid'];
			$application->integrator = $row['integrator'];
			$application->environment= $row['environment'];
			$application->offer      = $row['offer'];
			$application->id         = $row['id'];
			$application->description= $row['description'];
			
			return $application;
			
		} else {
			error_log("mysql erro: "+$this->db->error);
		}
		return null;
	}
	
	public function getAccountGuid() {
		
		$sql = "select * from ".$GLOBALS['db_applications']." where appid='".$this->appId."'";
		if ($result = $this->db->query($sql)) {
			$row = $result->fetch_assoc();
			error_log("guid = ".$row['guid']);
			return $row['guid'];
		} else {
			error_log("mysql erro: "+$this->db->error);
		}
		return null;
	}
	
	// Returns the row object identified by UUID, null if not found
	public function getSubscriberByDevice($uuid) {
		global $db_tablename;
		
		$subscriber=null;
		
		if ($result = $this->db->query("SELECT * FROM ".$db_tablename." WHERE uuids='" . $uuid ."' AND appid='".$this->appId."'")) {
			$row = $result->fetch_assoc();
			//error_log("row = ".var_export($row, true));
			if (!empty($row)) {
				$subscriber=new Subscriber();
				foreach($row as $key=>$value) {
					//error_log("getSubscriber::".$key."=>".$value);
					if ($key=='uuids' || $key=='verifiedProducts') {
						$subscriber->$key = explode(",",$value);
					}
					else {
						$subscriber->$key = $value;
					}
				}
			}
		}
		return $subscriber;
	}
	
	public function getSubscriberByName($user) {
		global $db_tablename;
		
		$subscriber=null;
		$sql = "SELECT * FROM ".$db_tablename." WHERE emailAddress='" . $user . "' AND appid='".$this->appId."'";
		error_log("getSubscriberByName::".$sql);
		
		if ($result = $this->db->query($sql )) {
			$row = $result->fetch_assoc();
			//error_log("row = ".var_export($row, true));
			if (!empty($row)) {
				$subscriber=new Subscriber();
				foreach($row as $key=>$value) {
					//error_log("getSubscriber::".$key."=>".$value);
					if ($key=='uuids' || $key=='verifiedProducts') {
						$subscriber->$key = explode(",",$value);
					}
					else {
						$subscriber->$key = $value;
					}
				}
			}
		}
		return $subscriber;
	}
	
	public function getSubscriberByAuthToken($authToken) {
		global $db_tablename;
		
		$subscriber=null;
		error_log("searching for authToken::".$authToken);
		$sql = "SELECT * FROM ".$db_tablename." WHERE authToken='" . $authToken . "' AND appid='".$this->appId."'";
		if ($result = $this->db->query($sql)) {
			$row = $result->fetch_assoc();
			//error_log("row = ".var_export($row, true));
			if (!empty($row)) {
				$subscriber=new Subscriber();
				foreach($row as $key=>$value) {
					//error_log("getSubscriber::".$key."=>".$value);
					if ($key=='uuids' || $key=='verifiedProducts') {
						$subscriber->$key = explode(",",$value);
					}
					else {
						$subscriber->$key = $value;
					}
				}
			}
		}
		else {
			error_log("not found");
		}
		return $subscriber;
	}
	
	public function createSubscriber($subscriber) {
		
		global $db_tablename;
		
		$uuids = implode(",",$subscriber->uuids);
		$firstName = $subscriber->firstName;
		$lastName  = $subscriber->lastName;
		$emailAddress = $subscriber->emailAddress;
		$password  = $subscriber->password; // it should already be MD5'd
		$substart  = $subscriber->subscriptionStart;
		$subrenew  = $subscriber->subscriptionRenew;
		$active    = $subscriber->active;
		$offer     = $subscriber->offer;
		$productid = $subscriber->productId;
		$authToken = $subscriber->authToken;
		$filter    = $subscriber->filter;
		$verifiedProducts = implode(",", $subscriber->verifiedProducts);
		
		$sql = "INSERT INTO ". $db_tablename." (firstName, lastname, emailAddress, password, subscriptionStart,  subscriptionRenew, uuids, active, appid, authToken, offer, productId, verifiedProducts, filter) VALUES ('".$firstName."', '".$lastName."', '".$emailAddress. "', '".$password. "', '".$substart. "', '".$subrenew."','".$uuids."','".$active."','".$this->appId."','".$authToken."','".$offer."','".$productId."','".$verifiedProducts."','".$filter."' )";
		error_log("createSubscriber::".$sql);
		if ($this->db->query($sql)) return $this->db->insert_id;
		else {
			error_log("mysql erro: "+$this->db->error);
			return false;
		}
	}
	
	public function removeSubscriberById($id) {
		global $db_tablename;
		return $this->db->query("delete from ".$db_tablename." WHERE id='".$id."'");
	}
	
	public function updateSubscriber($subscriber) {
		global $db_tablename;
		
		// Remove existing and replace with new
		$this->removeSubscriberById($subscriber->id);
		$subscriber->id = $this->createSubscriber($subscriber);
		
		$subscriber->id = $this->db->insert_id;
		return $subscriber;
	}
	
	// Updates the database to mark this UUID as belonging to an external print provider
	public function setIsIntegrator($uuid) {
		global $db_tablename;
		
		$subscriber = $this->getSubscriberByDevice($uuid);
		if (!empty($subscriber)) {
			$sql = "UPDATE ".$db_tablename." SET integrator='1' WHERE id='".$subscriber->id."'";
			error_log("updating UUID (".$uuid.") for '".$this->appId."'");
			error_log("sql=>".$sql);
			if (!$this->db->query($sql)) {
				error_log("error inserting uuid for integrator: ".$this->db->error);
			}
		}
		else {
			$sql ="INSERT INTO ".$db_tablename." (appid, uuids, integrator) VALUES ('".$this->appId."','".$uuid."','1')";
			error_log("registering UUID (".$uuid.") for '".$this->appId."'");
			error_log("sql=>".$sql);
			if (!$this->db->query($sql)) {
				error_log("error inserting uuid for integrator: ".$this->db->error);
			}
		}
	}
	
	public function applicationSearch($appid="", $adobeId="") {
		global $db_applications;
		
		$r = array();
		
		$selectors="";
		if (!empty($appid)) {
			$selectors .= " appid LIKE '%".$appid."%'";
		}
		if (!empty($adobeId)) {
			$selectors .= !empty($selectors)?" AND" : " "." adobeid LIKE '%".$adobeId."%'";
		}
		if (!empty($selectors)) {
			
			$sql = "SELECT * from ".$db_applications." WHERE ".$selectors;
			//echo $sql;
			if ($result = $this->db->query($sql)) {
				while ($row = $result->fetch_assoc()) {
					$application = new Application($row['appid']);
					$application->adobeid    = $row['adobeid'];
					$application->guid       = $row['guid'];
					$application->integrator = $row['integrator'];
					$application->environment    = $row['environment'];
					$application->offer      = $row['offer'];
					$application->id         = $row['id'];
					$application->description= $row['description'];
					
					array_push($r, $application);
				}
			}
		}
		return $r;
	}
	
	
	public function userSearch($firstName="", $lastName="", $emailAddress="") {
		global $db_tablename;
		
		$r = array();
		
		$selectors="";
		if (!empty($firstName)) {
			$selectors .= " firstName LIKE '".$firstName."'";
		}
		if (!empty($lastName)) {
			$selectors .= (!empty($selectors)?" AND" : " ")." lastName LIKE '".$lastName."'";
		}
		if (!empty($emailAddress)) {
			$selectors .= (!empty($selectors)?" AND" : " ")." emailAddress LIKE '%".$emailAddress."%'";
		}
		if (!empty($selectors)) {
			$sql = "SELECT * from ".$db_tablename." WHERE ".$selectors;
			
			if ($result = $this->db->query($sql)) {
				error_log("userSearch::count=".$result->num_rows);
				while ($row = $result->fetch_assoc()) {
					$subscriber=new Subscriber();
					foreach($row as $key=>$value) {
						if ($key=='uuids') {
							$subscriber->$key = explode(",",$value);
						}
						else {
							$subscriber->$key = $value;
						}
						$subscriber->appId = $row['appid']; // Ugh...
					}
					array_push($r, $subscriber);
				}
			}
			else {
				error_log("mysql erro: "+$this->db->error);
			}
		}
		
		return $r;
	}

}
?>