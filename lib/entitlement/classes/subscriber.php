<?php

include_once '../settings.php';
class Subscriber {
	// essentially mirros the columns in the database
	private $id="";
	private $emailAddress="";
	private $firstName="";
	private $lastName="";
	private $password="";
	private $subscriptionStart=null; // date
	private $subscriptionRenew=null; // date
	private $active=false;
	private $authToken="";
	private $uuids=array();
	private $appId="";
	private $offer="";
	private $productId="";
	private $filter="";
	private $verifiedProducts=array();
	
	
	public function register_productId($id) {
		$success = false;
		foreach($this->verifiedProducts as $productId) {
			if ($productId==$id) {
				$success = true;
			}
		}
		if (!$success) {
			array_push($this->verifiedProducts, $id);
			$success=true;
		}
		return $success;
	}
	
	public function register_uuid($id) {
		global $max_uuids;
		
		$success = false;
		foreach($this->uuids as $uuid) {
			if ($uuid==$id) {
				$success = true;
			}
		}
		if (!$success && count($this->uuids) < $max_uuids) {
			array_push($this->uuids, $id);
			$success=true;
		}
		return $success;
	}
	
	public function __get($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
	}
	
	public function __set($property, $value) {
		if (property_exists($this, $property)) {
			$this->$property = $value;
		}
	}
 
}
?>
