<?php
class Application {
	public $appId;
	
	public $guid;
	public $environment;
	public $integrator;
	public $adobeid;
	public $offer;
	public $description;
	
	public $id;
	
	public function __construct($appId) {
		$this->appId = $appId;
		$this->environment = "production";
		$this->description = "";
	}
}
?>