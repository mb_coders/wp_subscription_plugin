<?php
require_once '../settings.php';
define('WP_USE_THEMES', false);
require ('../../../../../wp-blog-header.php');

$appId        = $_REQUEST['appId'];
$appVersion   = $_REQUEST['appVersion'];
$authToken    = $_REQUEST['authToken'];

$isV2 = ($_SERVER['REQUEST_METHOD']=="POST") ? true : false;

$success = false;
$xml = simplexml_load_string("<result/>");

$entitlements = $xml->addChild("entitlements");

$user_query = new WP_User_Query( array( 'meta_key' => 'authtoken', 'meta_value' => $authToken ) );
$users = $user_query->get_results();
if (! empty($users) ) {
    $success=true;
    $user = $users[0];
    $id = $user->ID;

    $logger->logDebug("authToken:".$authToken);
    if (get_field('authtoken', $id)==$authToken) {
        $args = array(
            'post_type' => 'wsk_prenumeraty',
            'posts_per_page' => 1,
            'tax_query'=>array(
                array(
                    'taxonomy'=>'wsk_typ',
                    'field'=>'slug',
                    'terms'=>'cyfrowa'
                ),
                array(
                    'taxonomy'=>'wsk_active',
                    'field'=>'slug',
                    'terms'=>'tak'
                )
            ),
            'meta_query' => array(
                array(
                    'key' => 'klient',
                    'value' => '"' . $id . '"',
                    'compare' => 'LIKE'
                )
            )
        );
        $query = new WP_Query($args);
        if ($query->have_posts()){ $query->the_post();
            $folio = file_get_contents("php://input"); // $_POST['credentials'];
            $folioXML = simplexml_load_string($folio);
            $pierwszy_numer = get_field('pierwszy_numer', get_the_ID());
            $ilosc_wszystkich_numerow = get_field('ilosc_wszystkich_numerow', get_the_ID());

            // Pierwszy numer
            $p_numer_arr = explode('/', $pierwszy_numer); //[0] numer [1] rok
            $p_numer_number = $p_numer_arr[1] . $p_numer_arr[0];

            // Wyliczanie ostatniego numeru
            $new_number = $p_numer_arr[0] + $ilosc_wszystkich_numerow - 1;
            $count_day_in_year = (date('L', strtotime($p_numer_arr[1].'-0-0')))? 366 : 365;
            if ($new_number > $count_day_in_year){
                $new_number -= $count_day_in_year;
            }
            $last_numer_number = $p_numer_arr[1] . $new_number;

            foreach ($folioXML->folio as $folio) {
                $logger->logInfo("ProductId => " . $folio->productId);
                $logger->logInfo("Folio => " . print_r($folio, true));

                $productId = explode('.', $folio->productId); // [1] rok [2] numer
                $folio_number = $productId[1] . $productId[2];

                if ($folio_number >= $p_numer_number && $folio_number <= $last_numer_number) {
                    $productId = $folio->productId;
                    $productNode = $entitlements->addChild("productId", $productId);
                    if ($isV2) {
                        $productNode->addAttribute("subscriberType", "direct");
                        $productNode->addAttribute("subscriberId", $row['emailAddress'] );
                    }
                }
            }
        }else {
            $logger->logInfo("entitlements=>subscription doesn't exist");
        }
        wp_reset_query();

    }
    else {
        $logger->logInfo("entitlements=>subscription not active or authToken not set");
    }
}
else {
    $logger->logInfo("entitlements=>did not find authToken record");
}


$xml->addAttribute("httpResponseCode", $success?"200":"401");
header("Content-Type: application/xml");
echo $xml->asXML();
?>
