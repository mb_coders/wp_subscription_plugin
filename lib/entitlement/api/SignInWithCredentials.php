<?php

require_once "../settings.php";
require_once "../utils.php";
define('WP_USE_THEMES', false);
require ('../../../../../wp-blog-header.php');

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $appId = $_GET['appId'];
    $appVersion = $_GET['appVersion'];
    $uuid = $_GET['uuid'];
    $emailAddress = trim($_GET['emailAddress']);
    $password = trim($_GET['password']);
} else {
    $appId = $_REQUEST['appId'];
    $appVersion = $_REQUEST['appVersion'];
    $uuid = $_REQUEST['uuid'];
    $credentials = file_get_contents("php://input"); // $_POST['credentials'];
    $credentialsXML = simplexml_load_string($credentials);
    $emailAddress = trim($credentialsXML->emailAddress);
    $password = trim($credentialsXML->password);
    // $logger->logDebug("Credentails XML = " . $credentials);
    $logger->logDebug("V2 email/password = " . $emailAddress . " / " . md5($password));
}


$success = false;

if (validUsername($emailAddress)) {
    $authToken = createAuthToken($emailAddress);

    $user = get_user_by( 'email', $emailAddress );
    if ($user !== false) {
        $id = $user->ID;

        if (wp_check_password( $password, $user->data->user_pass, $id )) {

            $success = true;

            $logger->logInfo('Login success!');

            if (get_field('authtoken', $id) != $authToken) {
                // Store the new authToken
                update_field('authtoken', $authToken, $id);
                $logger->logDebug("inserting new authToken: " . $authToken);
            }
        } else {
            // passwords do not match.  Clear authToken
            $logger->logInfo("SignInWithCredentials=>incorrect password for user \"" . $emailAddress . "\".  Clearing authToken");
            update_field('authtoken', '', $id);
        }
    } else {
        $logger->logInfo("SignInWithCredentials=>No record for user '" . $emailAddress . "' exists");
    }
} else {
    $logger->logDebug("SignInWithCredentials=>username/email failed format requirements.");
}

header("Content-Type: application/xml");
$xml = simplexml_load_string("<result/>");
if ($success) {
    $xml->addAttribute("httpResponseCode", '200');
    $xml->addChild("authToken", $authToken);
} else {
    $xml->addAttribute("httpResponseCode", '401');
}
echo $xml->asXML();

?>
