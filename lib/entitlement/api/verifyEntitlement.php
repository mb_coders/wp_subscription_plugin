<?php
require_once '../settings.php';

$productId  = $_REQUEST['productId'];
$authToken  = $_REQUEST['authToken'];
$appId      = $_REQUEST['appId'];
$appVersion = $_REQUEST['appVersion'];

$responseCode="200";

$success=false;
if (mysql_connect($db_host, $db_user, $db_pass) && mysql_select_db($db_name)) {
	// Need to ensure that the account still is active
	$result = mysql_query("SELECT * FROM ".$db_tablename." WHERE authToken='" . $authToken . "'");
	if ($result) {
		$row = mysql_fetch_assoc($result);
		if ($row && $row['active']==1) {
			
			// Validate against the entitlement list
			$logger->logInfo("verifyEntitlement=>User \"".$row['emailAddress']."\" is requesting download of \"".$productId."\"");
			
			
			// Validate that the product is entitled.
			// we will simply call the 'entitlements' API to check.
			$path_parts = pathinfo($_SERVER['REQUEST_URI']);
			$c = curl_init("http://localhost".$path_parts['dirname']."/entitlements?authToken=".$authToken."&appVersion=".$appVersion."&appId=".$appId);
			if ($c)
			{
				curl_setopt($c,CURLOPT_RETURNTRANSFER, true);
				curl_setopt($c,CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($c,CURLOPT_TIMEOUT, 30);
				$result = curl_exec($c);
				curl_close($c);
				
				$entitled = new SimpleXMLElement($result);
				$nodes = $entitled->xpath("//productId[text()='".$productId."']");
				if (!empty($nodes)) $success = true;
			}
		}
	}
	else {
		$logger->logError("verifyEntitlement=>Unrecognized user \"".$authToken."\" is requesting download of \"".$productId."\"");
	}
}
else {
	$responseCode="403";
	$logger->logError("verifyEntitlement=>Could not connect to database");
}

$xml = simplexml_load_string("<result/>");
$xml->addAttribute("httpResponseCode", $responseCode);

$xml->addChild("entitled", $success ? "true" : "false");
header("Content-Type: application/xml");
echo $xml->asXML();
?>
