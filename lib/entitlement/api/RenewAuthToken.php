<?php

// Gather POST variables.  For ease of testing, we are agnostic of http method used.
require_once '../settings.php';
define('WP_USE_THEMES', false);
require ('../../../../../wp-blog-header.php');

$appId = $_REQUEST['appId'];
$appVersion = $_REQUEST['appVersion'];
$uuid = $_REQUEST['uuid'];
$authToken = $_REQUEST['authToken'];

$success = false;

$user_query = new WP_User_Query( array( 'meta_key' => 'authtoken', 'meta_value' => $authToken ) );
$users = $user_query->get_results();
if (! empty($users) ) {
    $user = $users[0];
    $id = $user->ID;

    if (get_field('authtoken', $id) == $authToken) {
        // We should probably check whether the input UUID matches one of the UUIDS in the db...but we will not.
        // See SignInWithCredentials for example.
        $logger->logDebug("renewing authtoken for '" . $user->email . "'");
        $success = true;
    } else {
        // clear authToken
        $logger->logDebug("account '" . $user->email . "' is not active");
        update_field('authtoken', '', $id);
    }
}

if ($success) {
    $xml = simplexml_load_string("<result />");
    $xml->addAttribute("httpResponseCode", "200");
    $xml->addChild("authToken", $authToken);
} else {
    // Not found
    $xml = simplexml_load_string("<result />");
    $xml->addAttribute("httpResponseCode", "401");
}
header("Content-Type: application/xml");
echo $xml->asXML();
?>