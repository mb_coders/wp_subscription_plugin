<?php

namespace WP_SUBSCRIPTION_PLUGIN;

/**
 * The code used on the frontend.
 */
class Frontend
{
    private $plugin_slug;
    private $version;
    private $option_name;
    private $settings;

    public function __construct($plugin_slug, $version, $option_name) {
        $this->plugin_slug = $plugin_slug;
        $this->version = $version;
        $this->option_name = $option_name;
        $this->settings = get_option($this->option_name);
    }

    public function order_item_product( $cart_item, $order_item ){
        if( isset( $order_item['startowy_numer'] ) ){
            $cart_item_meta['startowy_numer'] = $order_item['startowy_numer'];
        }
     
        return $cart_item;
    }

    public function get_item_data( $other_data, $cart_item ) {
        if ( isset( $cart_item['startowy_numer'] ) ){
            $other_data[] = array(
                'name' => __( 'Startowy numer', 'wsk-textdomain' ),
                'value' => sanitize_text_field( $cart_item['startowy_numer'] )
            );
        }

        return $other_data;
    }

    public function add_order_item_meta( $item_id, $values ){
        if ( ! empty( $values['startowy_numer'] ) ) {
            woocommerce_add_order_item_meta( $item_id, 'startowy_numer', $values['startowy_numer'] );
        }
    }

    public function get_cart_item_from_session( $cart_item, $values ) {
        if ( isset( $values['startowy_numer'] ) ){
            $cart_item['startowy_numer'] = $values['startowy_numer'];
        }
     
        return $cart_item;
    }

    public function before_add_to_cart_button(){
        if (!\has_term('prenumerata', 'product_cat')) return;

        $name = 'startowy_numer';
        $html = '<label>Wybierz numer startowy</label>';
        $html .= '<select id="'.$name.'" name="'.$name.'">';
        $first = get_field('najstarszy_numer', 'option');
        $first_arr = explode('/', $first);
        switch (WSK_TYPE) {
            case 'dziennik':
                $first_issue_date = strtotime(date('Y-m-d', mktime(0, 0, 0, 1, ($first_arr[0]), $first_arr[1])));
                $issue_date = $first_issue_date;
                while ($issue_date <= strtotime('+2 weeks')) {
                    $issue_day = date('z', $issue_date) + 1;
                    $issue_true_day = date('j', $issue_date);
                    $issue_month = date('n', $issue_date);
                    $issue_year = date('Y', $issue_date);

                    $issue_date = strtotime('+1 day', $issue_date);
                    $label = $issue_true_day.'/'.$issue_month.'/'.$issue_year;
                    $html .= '<option value="'.$issue_day.'/'.$issue_year.'">'.$label.'</option>';
                }

                break;
            
            case 'dwumiesiecznik':
                $first_issue_date = strtotime(date('Y-m', mktime(0, 0, 0, $first_arr[0], 1, $first_arr[1])));
                $issue_date = $first_issue_date;
                while ($issue_date <= strtotime('+1 year')) {
                    $issue_month = date('n', $issue_date);
                    $issue_month = floor($issue_month / 2) + 1;
                    $issue_year = date('Y', $issue_date);

                    $issue_date = strtotime('+2 month', $issue_date);
                    $label = $issue_month.'/'.$issue_year;
                    $html .= '<option value="'.$issue_month.'/'.$issue_year.'">'.$label.'</option>';
                }

                break;
        }
        $html .= '</select>';
        echo $html;
    }

    public function new_order_item( $cart_item, $product_id ){
        if( isset( $_POST['startowy_numer'] ) ) {
            $cart_item['startowy_numer'] = sanitize_text_field( $_POST['startowy_numer'] );
        }
     
        return $cart_item;
    }

    public function assets() {
        wp_enqueue_style($this->plugin_slug, plugin_dir_url(__FILE__).'css/frontend.css', [], $this->version);
        wp_enqueue_script($this->plugin_slug, plugin_dir_url(__FILE__).'js/frontend.js', ['jquery'], $this->version, true);
    }

    /**
     * Render the view using MVC pattern.
     */
    public function render() {

        // Model
        $settings = $this->settings;

        // Controller
        // Declare vars like so:
        // $var = $settings['slug'] ?? '';

        // View
        if (locate_template('partials/' . $this->plugin_slug . '.php')) {
            require_once(locate_template('partials/' . $this->plugin_slug . '.php'));
        } else {
            require_once plugin_dir_path(dirname(__FILE__)).'frontend/partials/view.php';
        }
    }
}
