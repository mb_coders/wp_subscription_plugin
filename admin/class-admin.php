<?php

namespace WP_SUBSCRIPTION_PLUGIN;

/**
 * The code used in the admin.
 */
class Admin
{
    private $plugin_slug;
    private $version;
    private $option_name;
    private $settings;
    private $settings_group;
    private $sub_controller;
    private $issues_controller;

    public function __construct($plugin_slug, $version, $option_name) {
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/controllers/subscriptions_controller.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/controllers/issues_controller.php';

        $this->plugin_slug = $plugin_slug;
        $this->version = $version;
        $this->option_name = $option_name;
        $this->settings = get_option($this->option_name);
        $this->settings_group = $this->option_name.'_group';
        $this->sub_controller = new Subscriptions();
        $this->issues_controller = new Issues();
    }

    function prolongation_sub_woocommerce_email( $email_classes ) {

	    // include our custom email class
	    require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-prolongation-order-email.php';

	    // add the email class to the list of email classes that WooCommerce loads
	    $email_classes['WC_Prolongation_Order_Email'] = new WC_Prolongation_Order_Email();

	    return $email_classes;

	}

    public function generate_issues() {
        if (!is_admin()) return;

        $last_issue = $this->issues_controller->get_last_issue('date');
        $current_issue = $this->issues_controller->get_current_issue('date');

        if ($last_issue === false){
            if ($current_issue === false){
                $current_issue = ['month' => date('n', strtotime("-1 month")), 'year' => date('Y', strtotime("-1 month"))]; //Pobieramy miesiąc wcześniej, gdyż później pobieramy kolejny numer przed dodaniem

                if ($current_issue['month'] % 2 == 0) {
                    $current_issue['month'] -= 1;
                }
            }

            $last_issue = $current_issue;
        }

        $count = 6 - $this->issues_controller->issues_diff($current_issue, $last_issue);
        $next_issue = $this->issues_controller->next_issue($last_issue, 'date');

        for ($i = 0; $i < $count; $i++) {
            $this->issues_controller->add($next_issue['month'], $next_issue['year']);
            $next_issue = $this->issues_controller->next_issue($next_issue, 'date');
        }
    }

    public function generate_orders_from_subs() {
        global $typenow;
        if ($typenow != 'wsk_prenumeraty') return;

        ?>
        <div class="wrap alignright">
            <form method="post" action="">
                <input type="hidden" name='generate_sub_orders' value="1"/>
                <select name="issue">
                    <?php
                        $month = date('n');
                        $issue_month = floor($month / 2) + 1;
                        $issue_year = date('Y');
                        $issue = ['month' => $issue_month, 'year' => $issue_year-1];

                        for($i = 1; $i <= 12; $i++):                            
                    ?>
                        <option value="<?php echo $issue['month'].'/'.$issue['year']; ?>"><?php echo $issue['month'].'/'.$issue['year']; ?></option>
                    <?php
                        $issue['month']++;
                        if ($issue['month'] > 6){
                            $issue['year']++;
                            $issue['month'] = 1;
                        }
                        endfor;
                    ?>
                </select>
                <input type="submit" name="generate_sub_orders_btn" id="generate_sub_orders" class="button" value="Wygeneruj etykiety" />
            </form>
        </div>
        <?php

        if (!empty($_POST['generate_sub_orders']) && $_POST['generate_sub_orders'] == 1) {
            $query = $this->sub_controller->search('papierowa', 'tak');

            if ($query->have_posts()) {
                while ($query->have_posts()){
                    $query->the_post();

                    $sub_id = get_the_ID();
                    $all = get_field('ilosc_wszystkich_numerow');
                    $sent = get_field('ilosc_wyslanych_numerow');

                    //Pobranie danych do zamówienia
                    $address = array(
                        'first_name' => get_field('imie'),
                        'last_name'  => get_field('nazwisko'),
                        'company'    => get_field('firma'),
                        'email'      => get_field('email'),
                        'address_1'  => get_field('adres_linia_1'),
                        'address_2'  => get_field('adres_linia_2'),
                        'city'       => get_field('miasto'),
                        'postcode'   => get_field('kod_pocztowy'),
                        'country'    => get_field('kraj')
                    );

                    if ($sent < $all) {
                        $first_issue = explode("/", get_field('pierwszy_numer'));
                        $first_issue = [ 'month' => $first_issue[0], 'year' => $first_issue[1] ];

                        $issue_gen = explode("/", $_POST['issue']);
                        $next_to_send_issue = ['month' => $first_issue['month'], 'year' => $first_issue['year']];
                        $next_to_send_issue['month'] += $sent;

                        while ($next_to_send_issue['month'] > 6) {
                            $next_to_send_issue['month'] -= 6;
                            $next_to_send_issue['year']++;
                        }

                        if ($next_to_send_issue['month'] != $issue_gen[0] || $next_to_send_issue['year'] != $issue_gen[1]){
                            continue;
                        }

                        $next_issue_id = $this->issues_controller->next_issue($next_to_send_issue, 'id');

                        //Wygenerowanie zamówienia
                        $order = wc_create_order();
                        $issue_wc_product = get_product( $next_issue_id );
                        $order->add_product( $issue_wc_product, 1);
                        $order->set_address( $address, 'billing' );
                        $order->set_address( $address, 'shipping' );

                        WC()->shipping->load_shipping_methods();
    					$shipping_methods = WC()->shipping->get_shipping_methods();
    					$selected_shipping_method = $shipping_methods[get_field('wysylka', $sub_id)];

	      				$item = new \WC_Order_Item_Shipping();
						$item->set_props( array(
							'method_title' => $selected_shipping_method->title,
							'method_id'    => $selected_shipping_method->id,
							'total'        => 0,
							'taxes'        => [],
							'order_id'     => $order->get_id(),
						) );

						$item->save();
						$order->add_item( $item );
                        
                        $enadawca = array();
                        $all_shipping_methods    = WC()->shipping()->get_shipping_methods();
                        $flexible_shipping_rates = $all_shipping_methods['flexible_shipping']->get_all_rates();

                        $ilosc_prenumerat = get_field('ilosc_prenumerat', $sub_id);
                        $kraj = get_field('kraj', $sub_id);

                        if ($kraj != 'PL'){
                            $id_for_shipping = 'flexible_shipping_3_1';
                        }elseif ($ilosc_prenumerat == 1){
                            $id_for_shipping = 'flexible_shipping_3_3';
                        }else{
                            $id_for_shipping = 'flexible_shipping_3_2';
                        }

                        $shipping_method = $flexible_shipping_rates[ $id_for_shipping ];
                        if ( $shipping_method['method_integration'] ==  \WPDesk_eNadawca_Shipping::SHIPPING_ID  ) {
                            $data   = array( 'usluga' => $shipping_method['enadawca_usluga'] );
                            $weight = 0;
                            if ( sizeof( $order->get_items() ) > 0 ) {
                                foreach ( $order->get_items() as $item ) {
                                    if ( $item['product_id'] > 0 ) {
                                        $_product = $order->get_product_from_item( $item );
                                        if ( ! $_product->is_virtual() ) {
                                            $weight += floatval( wc_get_weight( $_product->get_weight(), 'kg' ) ) * floatval( $item['qty'] );
                                        }
                                    }
                                }
                            }

                            if ( $weight > 0 ) {
                                $data['masa'] = $weight;
                            }

                            $data['pobranie_kwota'] = $order->get_total();

                            //Generowanie tytułu pobrania
                            $prod_quantity = $ilosc_prenumerat;
                            $prod_sku = $issue_wc_product->get_sku();
                            $pobranie_tytul = $prod_quantity.'x'.$prod_sku.',';
                            //~Generowanie tytułu pobrania

                            $data['opis'] = $pobranie_tytul;
                            $data['pobranie_konto'] = '';
                            $konta                  = array();
                            $bacs                   = new \WC_Gateway_BACS();
                            $accounts = $bacs->account_details;
                            if ( is_array( $accounts ) ) {
                                foreach ( $accounts as $account ) {
                                    $data['pobranie_konto'] = str_replace( ' ', '', $account['account_number'] );
                                    break;
                                }
                            }
                            if ( $shipping_method['enadawca_odbior_w_punkcie'] == 'yes' ) {
                                $data['odbior_w_punkcie'] = 1;
                                $data['punkt_odbioru']    = $_POST['enadawca_punkt_odbioru'];
                            }
                            $data['ubezpieczenie_kwota'] = 5000;

                            $data['zawartosc_global_express'] = 'Pozostałe';

                            $uslugi        = \WPDesk_eNadawca()->uslugi;
                            $class_name    = $uslugi[ $shipping_method['enadawca_usluga'] ]['class'];
                            $fs_parameters = $class_name::get_fs_parameters();
                            foreach ( $fs_parameters as $fs_parameter ) {
                                if ( isset( $shipping_method[ 'enadawca_' . $fs_parameter ] ) ) {
                                    $data[ $fs_parameter ] = $shipping_method[ 'enadawca_' . $fs_parameter ];
                                    if ( $data[ $fs_parameter ] == 'yes' ) {
                                        $data[ $fs_parameter ] = 1;
                                    }
                                    if ( $fs_parameter == 'opakowanie_gabaryt' ) {
                                        if ( $shipping_method['enadawca_opakowanie_gabaryt'] != '-' ) {
                                            $data['opakowanie'] = 1;
                                        }
                                    }
                                    if ( $fs_parameter == 'gabaryt_przesylka_biznesowa' ) {
                                        $data['gabaryt'] = $data[ $fs_parameter ];
                                        unset( $data[ $fs_parameter ] );
                                    }
                                }
                            }
                            $enadawca[ $id ] = $data;
                        }
                        if ( ! empty( $enadawca ) ) {
                            wpdesk_update_order_meta( $order, '_enadawca', $enadawca );
                            wpdesk_update_order_meta( $order, '_flexible_shipping_status', 'new' );
                        }

                        //Dodanie numery wysyłki
                        // $WPDesk_eNadawca_Class = \WPDesk_eNadawca_Class::getInstance();
                        // $WPDesk_eNadawca_Class->addShipment( $order->get_id(), $id );

                        // $enadawca = WPDesk_eNadawca();
                        // $enadawca->checkout_update_order_meta($order->get_id());

                        $order->calculate_totals();
                        $order->update_status('completed', 'Automatycznie wygenerowane z prenumeraty');

                        //Flaga, że jest to zamówienie z subskrypcji
                        update_field('wygenerowane_z_prenumeraty', true, $order->get_id());

                        //Aktualizacja wysłanych numerów
                        update_field('ilosc_wyslanych_numerow', get_field('ilosc_wyslanych_numerow', $sub_id) + 1, $sub_id);

                        //Aktualizacja wysłanych numerów (lista numerów)
                        add_row('wyslane_numery', ['numer' => $next_to_send_issue['month'] . '/' . $next_to_send_issue['year'] ], $sub_id);
                    }
                }
                ?>
                <div class="notice notice-success is-dismissible">
                    <p><?php _e( 'Etykiety wygenerowano pomyślnie!', 'wsk_domain' ); ?></p>
                </div>
                <?php
            }

            wp_reset_query();
        }
    }

    public function export_btn() {
        global $typenow;
        if ($typenow == 'wsk_prenumeraty') {
            ?>

            <div class="wrap alignright">
                <form method='post' action="">
                    <input type="hidden" name='export_csv' value="1"/>
                    <input type="hidden" name='wsk_typ' value="<?=$_GET['wsk_typ'];?>"/>
                    <input type="hidden" name='wsk_active' value="<?=$_GET['wsk_active'];?>"/>
                    <input type="hidden" name='wsk_dlugosc' value="<?=$_GET['wsk_dlugosc'];?>"/>
                    <input type="hidden" name='wsk_czestotliwosc' value="<?=$_GET['wsk_czestotliwosc'];?>"/>
                    <input type="hidden" name='wsk_wysylka' value="<?=$_GET['wsk_wysylka'];?>"/>
                    <input type="hidden" name='m' value="<?=$_GET['m'];?>"/>

                    <input type="submit" name='export' id="csvExport" class="button" value="Eksport do CSV"/>
            <?php

            if (!empty($_POST['export_csv']) && $_POST['export_csv'] == 1) {
                $filename = date('Y-m-d H-i-s') . 'export_prenumerat.csv';
                $file = fopen(plugin_dir_path( __FILE__ ) .'csv/' . $filename, 'w');

                $args = array('post_type'=>'wsk_prenumeraty');
                $tax_query = false;
                $relation = false;
                if ($_POST['wsk_typ'] != 0 ){
                    $wsk_typ = array(
                        'taxonomy'  =>  'wsk_typ',
                        'field'     =>  'slug',
                        'terms'     =>  $_POST['wsk_typ'],
                        'operator'  =>  'IN'

                    );
                    $tax_query = true;
                }
                if ($_POST['wsk_active'] != 0 ){
                    $wsk_active = array(
                        'taxonomy'  =>  'wsk_active',
                        'field'     =>  'slug',
                        'terms'     =>  $_POST['wsk_active'],
                        'operator'  =>  'IN'

                    );
                    $tax_query = true;
                }
                if ($tax_query){
                    $args['tax_query'] = array(
                        'relation'  =>  'AND',                        
                    );
                    if (!empty($wsk_typ)){
                        $args['tax_query'][] = $wsk_typ;
                    }
                    if (!empty($wsk_active)){
                        $args['tax_query'][] = $wsk_active;
                    }
                }
                $meta_query = false;
                if ($_POST['wsk_dlugosc'] != 0){
                    $meta_query = true;
                    $wsk_dlugosc = array(
                        'key'   => 'ilosc_wszystkich_numerow',
                        'value' =>  $_POST['wsk_dlugosc'],
                        'compare' => '=',
                    );
                }
                if ($_POST['wsk_czestotliwosc'] != 0){
                    $meta_query = true;
                    $wsk_czestotliwosc = array(
                        'key'   => 'czestotliwosc_wysylki',
                        'value' =>  $_POST['wsk_czestotliwosc'],
                        'compare' => '=',
                    );
                }
                if ($_POST['wsk_wysylka'] != 0){
                    $meta_query = true;
                    $wsk_wysylka = array(
                        'key'   => 'rodzaj_wysylki',
                        'value' =>  $_POST['wsk_wysylka'],
                        'compare' => '=',
                    );
                }
                if ($meta_query){
                    $args['meta_query'] = array();
                    if (!empty($wsk_dlugosc)){
                        $args['meta_query'][] = $wsk_dlugosc;
                    }
                    if (!empty($wsk_czestotliwosc)){
                        $args['meta_query'][] = $wsk_czestotliwosc;
                    }
                    if (!empty($wsk_wysylka)){
                        $args['meta_query'][] = $wsk_wysylka;
                    }
                }
                if ($_POST['m'] != 0){
                    $year = date('Y', strtotime($_POST['m']));
                    $month = date('n', strtotime($_POST['m']));
                    $args['year'] = $year;
                    $args['monthnum'] = $month;
                }
                $query = new \WP_Query($args);
                if ($query->have_posts()){
                    fputcsv($file, array('id', 'typ', 'aktywna', 'dlugosc', 'czestotliwosc', 'wysylka', 'data', 'id_zamowienia', 'user_imie', 'user_nazwisko', 'user_email', 'user_adres1', 'user_adres2', 'user_miasto', 'user_kod_pocztowy', 'pierwszy_numer', 'ilosc_wyslanych_numerow'));
                    $data = array();
                    while ($query->have_posts()) { $query->the_post();
                        $ID = get_the_ID();
                        $data[] = array(
                            $ID,
                            wp_get_post_terms( $ID, 'wsk_typ' )[0]->slug,
                            wp_get_post_terms( $ID, 'wsk_active' )[0]->slug,
                            get_field('ilosc_wszystkich_numerow'),
                            get_field('czestotliwosc_wysylki'),
                            get_field('rodzaj_wysylki'),
                            get_the_date('Y-m-d'),
                            get_field('zamowienie')[0],
                            get_field('imie'),
                            get_field('nazwisko'),
                            get_field('email'),
                            get_field('adres_linia_1'),
                            get_field('adres_linia_2'),
                            get_field('miasto'),
                            get_field('kod_pocztowy'),
                            get_field('pierwszy_numer'),
                            get_field('ilosc_wyslanych_numerow'),
                        );
                    }
                }
                wp_reset_query();
                 
                foreach ($data as $row) {
                    fputcsv($file, $row);
                }
                 
                fclose($file);

                ob_clean();

                header('Cache-Control: max-age=0');
                header('Cache-Control: private');
                header('Content-Description: File Transfer');
                header('Content-Type: application/force-download');
                header('Content-Disposition: attachment; filename=' . $filename);
                header('Content-Transfer-Encoding: binary');
                header('Content-Length: ' . filesize(plugin_dir_path( __FILE__ ) .'csv/' . $filename));

                $fp = fopen(plugin_dir_path( __FILE__ ) .'csv/' . $filename, 'rb');
                fpassthru($fp);
                exit;

                ?>
<!--                        <button class="button" type="button"><a href="/wp-content/plugins/wp-subscriptions-karonet/admin/csv/--><?php //echo $filename; ?><!--">Pobierz CSV</a></button>-->
                <?php
            }
            ?>
                </form>
            </div>
            <?php
        }
    }

    public function posts_filter( $query ) {
        global $pagenow;

        if (!is_admin()) return;

        $type = '';
        if (isset($_GET['post_type'])) {
            $type = $_GET['post_type'];
        }

        $meta_query = array();

        if ( 'wsk_prenumeraty' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['wsk_dlugosc']) && $_GET['wsk_dlugosc'] != '0') {
            $meta = array();
            $meta['key'] = 'ilosc_wszystkich_numerow';
            $meta['value'] = $_GET['wsk_dlugosc'];
            $meta['compare'] = '=';
            $meta_query[] = $meta;
        }
        if ( 'wsk_prenumeraty' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['wsk_czestotliwosc']) && $_GET['wsk_czestotliwosc'] != '0') {
            $meta = array();
            $meta['key'] = 'czestotliwosc_wysylki';
            $meta['value'] = $_GET['wsk_czestotliwosc'];
            $meta['compare'] = '=';
            $meta_query[] = $meta;
        }
        if ( 'wsk_prenumeraty' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['wsk_wysylka']) && $_GET['wsk_wysylka'] != '0') {
            $meta = array();
            $meta['key'] = 'rodzaj_wysylki';
            $meta['value'] = $_GET['wsk_wysylka'];
            $meta['compare'] = '=';
            $meta_query[] = $meta;
        }

        $query->set( 'meta_query', $meta_query );
    }

    public function orders_filter( $query ) {
        global $pagenow;

        if (!is_admin()) return;

        $type = '';
        if (isset($_GET['post_type'])) {
            $type = $_GET['post_type'];
        }

        $meta_query = array();

        if ( 'shop_order' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['wygenerowane_z_prenumeraty']) && $_GET['wygenerowane_z_prenumeraty'] != '0') {
            $meta = array();
            if ($_GET['wygenerowane_z_prenumeraty'] == 'true') {
                $meta['key'] = 'wygenerowane_z_prenumeraty';
                $meta['value'] = ($_GET['wygenerowane_z_prenumeraty'] == 'true')? true:false ;
                $meta['compare'] = '=';
            }else{
                $meta = array(
                    'relation'  => 'OR',
                    array(
                        'key'       => 'wygenerowane_z_prenumeraty',
                        'value'     => '0',
                        'compare'   => '=',
                    ),
                    array(
                        'key' => 'wygenerowane_z_prenumeraty',
                        'compare' => 'NOT EXISTS'
                    )
                );
            }

            $meta_query[] = $meta;
        }

        $query->set( 'meta_query', $meta_query );
    }

    public function save_bulk_edit_data( $post_id ) {
        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
             return $post_id;    

        if ( 'wsk_prenumeraty' == $_POST['post_type'] ) {
            if ( !current_user_can( 'edit_page', $post_id ) )
                return $post_id;
        }

        if (isset($_REQUEST['post_wyslany_numer'])) {
            $values = get_field('wyslane_numery', $post_id);
            $values[] = array('numer' => $_REQUEST['post_wyslany_numer']);
            update_field( 'wyslane_numery', $values, $post_id );
        
            return $_REQUEST['post_wyslany_numer'];
        }
    }

    public function add_quick_edit( $column_name, $post_type ) {
        if ($post_type != 'wsk_prenumeraty' || $column_name != 'nazwisko') return;
        ?>
        <fieldset class="inline-edit-col-left">
            <div class="inline-edit-col">
                <span class="title">Ostatni wysłany numer</span>
                <input type="hidden" name="wsk_wyslany_numer" id="wsk_wyslany_numer" value="" />
                <?php
                    $first_issue_date = strtotime('-2 weeks', time());
                    $fields = array();

                    $issue_date = $first_issue_date;
                    while ($issue_date <= strtotime('+2 weeks')) {
                        $issue_day = date('z', $issue_date) + 1;
                        $issue_true_day = date('j', $issue_date);
                        $issue_month = date('n', $issue_date);
                        $issue_year = date('Y', $issue_date);

                        $label = $issue_day.'/'.$issue_year.' ('.$issue_true_day.'/'.$issue_month.'/'.$issue_year.')';

                        $fields[$issue_day.'/'.$issue_year] = $label;

                        $issue_date = strtotime('+1 day', $issue_date);
                    }
                ?>
                <select name='post_wyslany_numer' id='post_wyslany_numer'>
                    <option class='numer-option' value='0'>None</option>
                    <?php 
                        foreach ($fields as $key => $field) {
                            echo "<option class='numer-option' value='{$key}'>{$field}</option>\n";
                        }
                    ?>
                </select>
            </div>
        </fieldset>
        <?php
    }

    public function remove_row_actions( $actions ) {
        if( is_post_type_archive('wsk_prenumeraty') ){
            unset( $actions['view'] );
            unset( $actions['inline hide-if-no-js'] );
        }
        return $actions;
    }

    public function create_prenumerata( $order_id ) {
    	$order = wc_get_order( $order_id );
        $order_data = $order->get_data();
		$items = $order->get_items();
		$ilosc_gazet = 0;
		$p_terms = array();
		foreach ( $items as $item_id => $item ) {
			$product_id = $item->get_product_id();
			$product = new \WC_Product( $product_id );
		    $product_terms = wp_get_post_terms( $product_id, 'wsk_typ' );
		    
		    foreach ($product_terms as $term) {
		    	$p_terms[$term->slug] = $item->get_quantity();
		    }
		    $tmp_ilosc_gazet = 6; //get_post_meta( $item->get_variation_id(), '_wsk_ilosc_gazet', true );
		    if ($tmp_ilosc_gazet) {
		    	$ilosc_gazet += $tmp_ilosc_gazet;
		    }

            $startowy_numer = wc_get_order_item_meta( $item_id, 'startowy_numer', true );	
		}

		//shipping
		foreach( $order->get_items( 'shipping' ) as $item_id => $shipping_item_obj ){
			$shipping_method_id = $shipping_item_obj->get_method_id();
		}

        $active = 'tak';
        $meta_key = 'email';
        $meta_value = $order_data['billing']['email'];
        foreach ($p_terms as $typ => $ilosc) {
        	if ($typ != 'cyfrowa' && $typ != 'papierowa') continue;

            $query = $this->sub_controller->search($typ, $active, $meta_key, $meta_value);

            if (!$order_data['shipping']['first_name']) {
                $order_data['shipping'] = $order_data['billing'];
            }

            if ($query->have_posts()){ $query->the_post();
                $sub_field = array(
                    'zamowienie' => $order_id,
                    'ilosc_wszystkich_numerow' => get_field('ilosc_wszystkich_numerow', $post_id) + $ilosc_gazet,
                    'klient' => $order_data['customer_id'],
                    'imie' => $order_data['shipping']['first_name'],
                    'nazwisko' => $order_data['shipping']['last_name'],
                    'firma' => $order_data['shipping']['company'],
                    'adres_linia_1' => $order_data['shipping']['address_1'],
                    'adres_linia_2' => $order_data['shipping']['address_2'],
                    'miasto' => $order_data['shipping']['city'],
                    'kod_pocztowy' => $order_data['shipping']['postcode'],
                    'kraj' => $order_data['shipping']['country'],
                    'email' => $order_data['shipping']['email'],
                    'wysylka' => $shipping_method_id,
                );
                $this->sub_controller->update($post->ID, $typ, $active, $sub_fields);
            }else{
                $sub_fields = array(
                    'zamowienie' => $order_id,
                    'pierwszy_numer' => $startowy_numer,
                    'ilosc_wszystkich_numerow' => $ilosc_gazet,
                    'ilosc_wyslanych_numerow' => 0,
                    'klient' => $order_data['customer_id'],
                    'imie' => $order_data['shipping']['first_name'],
                    'nazwisko' => $order_data['shipping']['last_name'],
                    'firma' => $order_data['shipping']['company'],
                    'adres_linia_1' => $order_data['shipping']['address_1'],
                    'adres_linia_2' => $order_data['shipping']['address_2'],
                    'miasto' => $order_data['shipping']['city'],
                    'kod_pocztowy' => $order_data['shipping']['postcode'],
                    'kraj' => $order_data['shipping']['country'],
                    'email' => $order_data['shipping']['email'],
                    'wysylka' => $shipping_method_id,
                    'ilosc_prenumerat' => $ilosc,
                );

                $this->sub_controller->add($typ, $active, $sub_fields);
            }
            wp_reset_query();    
        }
        
    }

    public function new_filters() {
		global $typenow;
		global $wp_query;
		
		if ( $typenow == 'wsk_prenumeraty' ) {
			$typy = get_terms(['taxonomy'=>'wsk_typ', 'hide_empty'=>false]); 
			$current_typ = 'all';
			$active = get_terms(['taxonomy'=>'wsk_active', 'hide_empty'=>false]); 
			$current_active = 'all';
            $dlugosci = array(                
                (object)array('slug'=>30,'name'=>30),
                (object)array('slug'=>91,'name'=>91),
                (object)array('slug'=>183,'name'=>183),
                (object)array('slug'=>365,'name'=>365),
                );
            $current_dlugosc = 'all';
            $czest = array(
                (object)array('slug'=>1,'name'=>'raz w tygodniu (piątki)'),
                (object)array('slug'=>3,'name'=>'trzy razy w tygodniu (poniedziałki, środy i piątki)'),
                );
            $current_czest = 'all';
            $wysylka = array(
                (object)array('slug'=>'ekonomiczna','name'=>'ekonomiczna'),
                (object)array('slug'=>'priorytetowa','name'=>'priorytetowa'),
                );
            $current_wysylka = 'all';
			
			if( isset( $_GET['wsk_typ'] ) ) {
				$current_typ = $_GET['wsk_typ'];
			}
			if( isset( $_GET['wsk_active'] ) ) {
				$current_active = $_GET['wsk_active'];
			}
            if( isset( $_GET['wsk_dlugosc'] ) ) {
                $current_dlugosc = $_GET['wsk_dlugosc'];
            }
            if( isset( $_GET['wsk_czestotliwosc'] ) ) {
                $current_czest = $_GET['wsk_czestotliwosc'];
            }
            if( isset( $_GET['wsk_wysylka'] ) ) {
                $current_wysylka = $_GET['wsk_wysylka'];
            }

			$this->print_filter('wsk_typ', $current_typ, $typy, 'Wszystkie typy');
			$this->print_filter('wsk_active', $current_active, $active, 'Aktywne prenumeraty');
            //$this->print_filter('wsk_dlugosc', $current_dlugosc, $dlugosci, 'Długość prenumeraty');
            //$this->print_filter('wsk_czestotliwosc', $current_czest, $czest, 'Częstotliwość wysyłki');
            //$this->print_filter('wsk_wysylka', $current_wysylka, $wysylka, 'Rodzaj wysyłki');
		}

        if ( $typenow == 'shop_order' ) {
            $opcje = array(                
                (object)array('slug'=>'true','name'=>'Tak'),
                (object)array('slug'=>'false','name'=>'Nie'),
                );
            $current_gen = 'all';
            if( isset( $_GET['wygenerowane_z_prenumeraty'] ) ) {
                $current_gen = $_GET['wygenerowane_z_prenumeraty'];
            }
            $this->print_filter('wygenerowane_z_prenumeraty', $current_gen, $opcje, 'Wygenerowane z prenumeraty');
        }
    }

    private function print_filter($name, $current, $args, $all) {
    	?>
    	<select name="<?php echo $name; ?>" id="<?php echo $name; ?>">
			<option value="0" <?php selected( 'all', $current ); ?>><?php echo $all; ?></option>
			<?php foreach( $args as $term ): ?>
			  <option value="<?php echo $term->slug; ?>" <?php selected( $term->slug, $current ); ?>><?php echo $term->name; ?></option>
			<?php endforeach; ?>
		</select>
		<?php
    }

    public function add_custom_general_fields ( $loop, $variation_data, $variation ) {
        global $woocommerce, $post;

        woocommerce_wp_text_input( 
            array( 
                'id'          => '_wsk_ilosc_gazet[' . $variation->ID . ']', 
                'label'       => __( 'Ilość numerów', 'woocommerce' ), 
                'placeholder' => 'np. 12',
                'desc_tip'    => 'true',
                'description' => __( 'Wpisz ilość numerów jaką posiada ta prenumerata.', 'woocommerce' ),
                'value'       => get_post_meta( $variation->ID, '_wsk_ilosc_gazet', true ),
                'custom_attributes' => array(
                            'step'  => 'any',
                            'min'   => '0'
                        ) 
            )
        );
    }

    public function add_custom_general_fields_save ( $post_id ) {
        $wsk_ilosc_gazet = $_POST['_wsk_ilosc_gazet'][ $post_id ];
        
        if( !empty( $wsk_ilosc_gazet ) ){
            update_post_meta( $post_id, '_wsk_ilosc_gazet', esc_attr( $wsk_ilosc_gazet ) );
        }
    }

    public function update_select_issues ( $field ) {
        // reset choices
        $field['choices'] = array();

        switch (WSK_TYPE) {
            case 'dziennik':
                $first_issue_date = strtotime('-2 weeks', time());

                $issue_date = $first_issue_date;
                while ($issue_date <= strtotime('+2 weeks')) {
                    $issue_day = date('z', $issue_date) + 1;
                    $issue_true_day = date('j', $issue_date);
                    $issue_month = date('n', $issue_date);
                    $issue_year = date('Y', $issue_date);

                    $label = $issue_day.'/'.$issue_year.' ('.$issue_true_day.'/'.$issue_month.'/'.$issue_year.')';

                    $field['choices'][$issue_day.'/'.$issue_year] = $label;

                    $issue_date = strtotime('+1 day', $issue_date);
                }

                break;
            
            case 'dwumiesiecznik':
                $month = date('n');
                
                $issue_number = ($month % 2 == 0)? $month -1 : $month;
                $issue_year = date('Y') - 1; //Rok do tyłu, aby był wybór starszych numerów

                for ($i=0; $i<=12; $i++) {
                    $field['choices'][$issue_number.'/'.$issue_year] = $issue_number.'/'.$issue_year;

                    $issue_number += 1;
                    if ($issue_number >= 7) {
                        $issue_number = 1;
                        $issue_year++;
                    }
                }

                break;
        }

        // return the field
        return $field;
    }

    public function acf_options () {
        if (function_exists('acf_add_options_page')) {
            $page = acf_add_options_page(array(
                'menu_title' => 'Ustawienia prenumerat',
                'menu_slug' => 'wsk-settings',
                'capability' => 'edit_posts',
                'redirect' => false
            ));
        }
    }

    public function sortable_columns ( $columns ) {
        return $this->custom_columns_labels($columns);
    }

    public function custom_columns_data ( $column, $post_id ) {
        switch ( $column ) {
            case 'ilosc_numerow' :
                echo get_field('ilosc_wszystkich_numerow', $post_id);
                break;
            case 'ilosc_wyslanych_numerow' :
                echo get_field('ilosc_wyslanych_numerow', $post_id);
                break;
            case 'ostatni_numer' :
                $ostatni = end( get_field( 'wyslane_numery', $post_id ) );
                echo ($ostatni)? $ostatni['numer'] : 'Brak';
                break;
            case 'zamowienie' :
                echo '<a href="'.get_edit_post_link (get_field('zamowienie', $post_id)[0] ).'">Edytuj zamówienie</a>'; 
                break;
            case 'imie' :
                echo get_field('imie', $post_id);
                break;
            case 'nazwisko' :
                echo get_field('nazwisko', $post_id);
                break;
            case 'email' :
                echo get_field('email', $post_id);
                break;
            case 'adres_linia_1' :
                echo get_field('adres_linia_1', $post_id);
                break;
            case 'adres_linia_2' :
                echo get_field('adres_linia_2', $post_id);
                break;
            case 'miasto' :
                echo get_field('miasto', $post_id);
                break;
            case 'kod_pocztowy' :
                echo get_field('kod_pocztowy', $post_id);
                break;
        }
    }

    public function order_columns_data ( $column, $post_id ) {
        switch ( $column ) {
            case 'wygenerowane_z_prenumeraty':
                echo get_field('wygenerowane_z_prenumeraty', $post_id) ? 'Tak' : 'Nie';
                break;
        }
    }

    public function custom_columns_labels ( $columns ) {
        unset( $columns['author'] );
        unset( $columns['title'] );
        $columns['ilosc_numerow'] = __( 'Ilość numerów', 'wsk_domain' );
        $columns['ilosc_wyslanych_numerow'] = __( 'Ilość wysłanych numerów', 'wsk_domain' );
        $columns['ostatni_numer'] = __( 'Ostatni numer', 'wsk_domain' );
        $columns['zamowienie'] = __( 'Zamówienie', 'wsk_domain' );
        $columns['imie'] = __( 'Imię', 'wsk_domain' );
        $columns['nazwisko'] = __( 'Nazwisko', 'wsk_domain' );
        $columns['email'] = __( 'Email', 'wsk_domain' );
        $columns['adres_linia_1'] = __( 'Adres linia 1', 'wsk_domain' );
        $columns['adres_linia_2'] = __( 'Adres linia 2', 'wsk_domain' );
        $columns['miasto'] = __( 'Miasto', 'wsk_domain' );
        $columns['kod_pocztowy'] = __( 'Kod pocztowy', 'wsk_domain' );

        return $columns;
    }

    public function orders_columns_labels ( $columns ) {
        $columns['wygenerowane_z_prenumeraty'] = __( 'Wygenerowane z prenumeraty', 'wsk_domain' );

        return $columns;
    }

    public function acf_load( $paths ) {
    
        // remove original path (optional)
        unset($paths[0]);
        
        
        // append path
        $paths[] = plugin_dir_path(__FILE__) . 'acf-json';
        
        
        // return
        return $paths;
        
    }
    
    public function acf_save( $path ) {
    
        // update path
        $path = plugin_dir_path(__FILE__) . 'acf-json';
        
        
        // return
        return $path;
        
    }

    public function check_config() {
        $accept_types = unserialize(ACCEPTABLE_TYPES);
        if ( ! in_array( WSK_TYPE, $accept_types ) ) {
            deactivate_plugins( plugin_basename( __FILE__ ) );
            wp_die( 'Źle skonfigurowano ustawienia w module WP SUBSCRIPTIONS KARONET. Skontaktuj się z deweloperem.' );
        }
    }

    /*
     *
     */
    public function reg_new_wsk_taxonomy() {
    	/* Typy dla produktów i prenumerat */
        $labels = array(
            'name'                       => 'Typy',
            'singular_name'              => 'Typ',
            'menu_name'                  => 'Typ',
            'all_items'                  => 'Wszystkie Typy',
            'parent_item'                => 'Rodzic typu',
            'parent_item_colon'          => 'Rodzic typu:',
            'new_item_name'              => 'Nowa nazwa typu',
            'add_new_item'               => 'Dodaj nowy typ',
            'edit_item'                  => 'Edytuj typ',
            'update_item'                => 'Aktualizuj typ',
            'separate_items_with_commas' => 'Oddziel typy przecinkiem',
            'search_items'               => 'Szukaj typów',
            'add_or_remove_items'        => 'Dodaj lub usuń typy',
            'choose_from_most_used'      => 'Wybierz z najczęściej wybieranych typów',
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
        );
        register_taxonomy( 'wsk_typ', ['product', 'wsk_prenumeraty'], $args );

        /* Aktywna - prenumeraty */
        $labels = array(
            'name'                       => 'Aktywna',
            'singular_name'              => 'Aktywna',
            'menu_name'                  => 'Aktywna',
            'all_items'                  => 'Wszystkie kategorie',
            'parent_item'                => 'Rodzic kategorii',
            'parent_item_colon'          => 'Rodzic kategorii:',
            'new_item_name'              => 'Nowa nazwa kategorii',
            'add_new_item'               => 'Dodaj nową kategorię',
            'edit_item'                  => 'Edytuj kategorię',
            'update_item'                => 'Aktualizuj kategorię',
            'separate_items_with_commas' => 'Oddziel kategorie przecinkiem',
            'search_items'               => 'Szukaj kategorii',
            'add_or_remove_items'        => 'Dodaj lub usuń kategorie',
            'choose_from_most_used'      => 'Wybierz z najczęściej wybieranych kategorii',
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
        );
        register_taxonomy( 'wsk_active', ['wsk_prenumeraty'], $args );
    }

    /*
     *
     */
    public function new_wsk_terms() {
    	// Typy
        wp_insert_term(
          'Prenumerata papierowa', // the term 
          'wsk_typ', // the taxonomy
          array(
            'description'=> 'Prenumerata papierowa',
            'slug' => 'papierowa',
          )
        );

        wp_insert_term(
          'Prenumerata cyfrowa', // the term 
          'wsk_typ', // the taxonomy
          array(
            'description'=> 'Prenumerata cyfrowa',
            'slug' => 'cyfrowa',
          )
        );

        wp_insert_term(
          'Numer magazynu', // the term 
          'wsk_typ', // the taxonomy
          array(
            'description'=> 'Numer magazynu',
            'slug' => 'numer-magazynu',
          )
        );

        // Aktywne
		wp_insert_term(
          'Tak', // the term 
          'wsk_active', // the taxonomy
          array(
            'description'=> 'Aktywna',
            'slug' => 'tak',
          )
        );

        wp_insert_term(
          'Nie', // the term 
          'wsk_active', // the taxonomy
          array(
            'description'=> 'Nieaktywna',
            'slug' => 'nie',
          )
        );        
    }

    public function reg_post_type_wsk_prenumeraty() {
        register_post_type( 'wsk_prenumeraty',
            array(
              'labels' => array(
                'name' => __( 'Prenumeraty' ),
                'singular_name' => __( 'Prenumerata' )
              ),
              'public' => true,
              'has_archive' => true,
            )
        );
    }

    /**
     * Generate settings fields by passing an array of data (see the render method).
     *
     * @param array $field_args The array that helps build the settings fields
     * @param array $settings   The settings array from the options table
     *
     * @return string The settings fields' HTML to be output in the view
     */
    private function custom_settings_fields($field_args, $settings) {
        $output = '';

        foreach ($field_args as $field) {
            $slug = $field['slug'];
            $setting = $this->option_name.'['.$slug.']';
            $label = esc_attr__($field['label'], 'wp-subscriptions-karonet');
            $output .= '<h3><label for="'.$setting.'">'.$label.'</label></h3>';

            if ($field['type'] === 'text') {
                $output .= '<p><input type="text" id="'.$setting.'" name="'.$setting.'" value="'.$settings[$slug].'"></p>';
            } elseif ($field['type'] === 'textarea') {
                $output .= '<p><textarea id="'.$setting.'" name="'.$setting.'" rows="10">'.$settings[$slug].'</textarea></p>';
            }
        }

        return $output;
    }

    public function assets() {
        wp_enqueue_style($this->plugin_slug, plugin_dir_url(__FILE__).'css/admin.css', [], $this->version);
        wp_enqueue_script($this->plugin_slug, plugin_dir_url(__FILE__).'js/admin.js', ['jquery'], $this->version, true);
    }

    public function register_settings() {
        register_setting($this->settings_group, $this->option_name);
    }

    public function add_menus() {
        $plugin_name = Info::get_plugin_title();
        add_submenu_page(
            'options-general.php',
            $plugin_name,
            $plugin_name,
            'manage_options',
            $this->plugin_slug,
            [$this, 'render']
        );
    }

    /**
     * Render the view using MVC pattern.
     */
    public function render() {

        // Generate the settings fields
        $field_args = [
            // [
            //     'label' => 'Text Label',
            //     'slug'  => 'text-slug',
            //     'type'  => 'text'
            // ],
            // [
            //     'label' => 'Textarea Label',
            //     'slug'  => 'textarea-slug',
            //     'type'  => 'textarea'
            // ]
        ];

        // Model
        $settings = $this->settings;

        // Controller
        $fields = $this->custom_settings_fields($field_args, $settings);
        $settings_group = $this->settings_group;
        $heading = Info::get_plugin_title();
        $submit_text = esc_attr__('Submit', 'wp-subscriptions-karonet');

        // View
        require_once plugin_dir_path(dirname(__FILE__)).'admin/partials/view.php';
    }
}
