<?php
/**
 * Plugin Name:       WP Subscriptions KaroNet
 * Description:       Plugin do subskrypcji
 * Version:           1.0
 * Author:            KaroNet
 * Author URI:        http://karo-net.pl
 * Text Domain:       wp-subscriptions-karonet
 */

namespace WP_SUBSCRIPTION_PLUGIN;

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

// The class that contains the plugin info.
require_once plugin_dir_path(__FILE__) . 'includes/class-info.php';

// The file with configs
require_once plugin_dir_path(__FILE__) . 'config.php';

/**
 * The code that runs during plugin activation.
 */
function activation() {
    require_once plugin_dir_path(__FILE__) . 'includes/class-activator.php';
    Activator::activate();
}
register_activation_hook(__FILE__, __NAMESPACE__ . '\\activation');

/**
 * Run the plugin.
 */
function run() {
    require_once plugin_dir_path(__FILE__) . 'includes/class-plugin.php';
    $plugin = new Plugin();
    $plugin->run();
}

run();
